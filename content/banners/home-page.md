---
template: banner
path: /
pageName: Home Page
bannersList:
  - buttonTitle: View More
    description: Make your production system into perfection with high-quality proficiency.
    href: '#'
    image: 'https://ucarecdn.com/6d1de0c4-2b98-46ec-8dc8-272d0efbc871/'
    title: '**Create** outstanding products with continuous support'
    video: 'https://www.youtube.com/watch?v=gYN-hTKSQ1k'
  - description: >-
      Bring the premium value united with a deep understanding of customers'
      demands.
    image: 'https://ucarecdn.com/6d4d4a3e-d328-4c8f-a600-46cf9610141f/'
    title: Bring **solutions** matching users' demands
  - description: Technological products simplify people's lives with optimized solutions.
    image: 'https://ucarecdn.com/5efea095-ba1c-4c39-85d9-ebf947fe930f/'
    title: '**Connect** the physical and the **digital** world'
---

