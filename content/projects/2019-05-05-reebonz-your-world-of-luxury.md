---
template: project
isHighlight: false
name: Reebonz
title: REEBONZ - Your World of Luxury
client: 1560408373
projects:
  - 1569236535
about:
  title: About Kuuho
  description: >-
    Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.Praesent
    sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus magna
    justo, lacinia eget consectetur sed, convallis at tellus. Quisque velit
    nisi, pretium ue nec, egestas non nisi. Sed porttitor lectus nibh lectus
    nibh.Quisque velit nisi, Quisque velit nisi, pretium ue nec, egestas non
    nisi. Sed porttitor lectus nibh lectus nibh.Quisque velit nisi, pretium ue
    nec, egestas non nisi. Sed porttitor lectus nibh lectus nibh.
  image: 'https://ucarecdn.com/8b9b2889-6cb0-4db9-ac14-ed74f536d506/'
quote:
  content: >-
    With one touch, Kuuho hepls you provide full informtion to the rescue forces
    and the entire community about a danger and insecurity you are facing.
banners:
  - description: >-
      Offering a user-friendly interface and range of useful features, KuuHo app
      has several features to help increase your personal safety.
    image: 'https://ucarecdn.com/27b98021-d4b3-448a-b0dd-86e4887b7b90/'
projectInfo:
  android: /
  title: Get the app!
  description: >-
    Pellentesque consequat placerat bibendum. Cras congue leo quis the ultrices.
    Pellentesque molestie nisl nec justo tincidunt fermentum. Naenenatis risus
    mauris, condimentum dapibus nibh.
  image: 'https://ucarecdn.com/321ae82b-119e-4f44-be5c-974072cc4ac7/'
  ios: /
  web: /
subTitle: >-
  Reebonz is a trusted online platform for buying and selling the widest range
  of luxury products. With an exciting lineup of coveted designer brands,
  personalised services, and buying and selling options, we make luxury
  accessible by giving you more from the world of luxury. Our seamless shopping
  experience ensures that you are assured of getting more when you buy and sell
  with us.  Sign up now as a Reebonz member and step into your world of luxury.
links:
  android: 'https://play.google.com/store/apps/details?id=com.reebonz.fashion&hl=en'
  ios: >-
    https://itunes.apple.com/sg/app/reebonz-your-world-of-luxury/id377175827?mt=8
  web: 'http://reebonz.com'
contactInfo:
  address: Viet Nam
  email: anh.doan@reebonz.com
  phoneNubmer: '+84789472011'
logo: 'https://res.cloudinary.com/csmenouvo/image/upload/v1557049463/reebonzlogo.jpg'
thumbnail: >-
  https://res.cloudinary.com/csmenouvo/image/upload/v1557049522/Screen_Shot_2019-05-05_at_4.45.05_PM.png
screenShort:
  - >-
    https://res.cloudinary.com/csmenouvo/image/upload/v1557049463/reebonzlogo.jpg
  - >-
    https://res.cloudinary.com/csmenouvo/image/upload/v1557049522/Screen_Shot_2019-05-05_at_4.45.05_PM.png
draft: false
date: 2019-05-05T09:39:44.145Z
endDate: 2019-05-05T09:39:44.218Z
description: >-
  Reebonz is a trusted online platform for buying and selling the widest range
  of luxury products. With an exciting lineup of coveted designer brands,
  personalised services, and buying and selling options, we make luxury
  accessible by giving you more from the world of luxury. Our seamless shopping
  experience ensures that you are assured of getting more when you buy and sell
  with us.  Sign up now as a Reebonz member and step into your world of luxury.
category:
  - WEB
intro:
  - title: What is Reebonz?
    description: >-
      Reebonz is a trusted online platform for buying and selling a wide range
      of luxury products. We make luxury accessible by giving members more from
      the world of luxury, in terms of our products, personal services,
      different ways to buy and sell, membership rewards, and personalised
      recommendations and knowledge.  With our seamless shopping experience,
      customers can always be assured of getting more when they buy and sell*
      with us.  *Reebonz's selling platform is only available in Singapore,
      Malaysia, Indonesia, Hong Kong, Taiwan and Australia.
feature:
  - title: What is Reebonz?
    description: >-
      Reebonz is a trusted online platform for buying and selling a wide range
      of luxury products. We make luxury accessible by giving members more from
      the world of luxury, in terms of our products, personal services,
      different ways to buy and sell, membership rewards, and personalised
      recommendations and knowledge.  With our seamless shopping experience,
      customers can always be assured of getting more when they buy and sell*
      with us.  *Reebonz's selling platform is only available in Singapore,
      Malaysia, Indonesia, Hong Kong, Taiwan and Australia.
service:
  - title: What is Reebonz?
    description: >-
      Reebonz is a trusted online platform for buying and selling a wide range
      of luxury products. We make luxury accessible by giving members more from
      the world of luxury, in terms of our products, personal services,
      different ways to buy and sell, membership rewards, and personalised
      recommendations and knowledge.  With our seamless shopping experience,
      customers can always be assured of getting more when they buy and sell*
      with us.  *Reebonz's selling platform is only available in Singapore,
      Malaysia, Indonesia, Hong Kong, Taiwan and Australia.
    listService: []
---
Reebonz is a trusted online platform for buying and selling the widest range of luxury products. With an exciting lineup of coveted designer brands, personalised services, and buying and selling options, we make luxury accessible by giving you more from the world of luxury. Our seamless shopping experience ensures that you are assured of getting more when you buy and sell with us.  Sign up now as a Reebonz member and step into your world of luxury.
