---
template: blog
hightlight: true
thumbnail: 'https://ucarecdn.com/5ae6a9f6-ae65-4757-a675-cecb08473d86/'
title: 'Back To Uni Laptops, Here Are My 3 Favorites'
tags:
  - WEB
author: 1559203861
draft: false
date: 2019-09-17T07:44:01.105Z
description: >-
  If you have any questions regarding my blog or just want to get in touch, I
  would be more than happy to hear
---


## Essential solutions

In 2009, Tran Hanh Trang completed a double bachelor’s degree in Computing and Finance at Monash University. During her school time, she had opportunities to work as a freelance in some website development projects, which brought her to passion about tech.

Trang and her husband, Pham Si Nguyen both have a passion for contributing to the growth of our hometown and country as well as the knowledge and experiences gained after long-term training in the developing environment. Therefore, they decided to leave Australia and to come back and establish a Technology company in Da Nang. In the beginning, there were only a few people. After 8 years of establishment, Enouvo IT Solutions now has up to 50 talents, trusted by local and foreign authorities to partner and develop technology applications.

![Enouvo](https://ucarecdn.com/b4e2e89a-3351-4d67-bf0d-ee12d151c5b7/)

## Supports for Vietnamese startups

As a start-up, EIS clearly knows the difficulties when starting a business. They not only offer start-ups or small businesses a working space full of services but also support in solving problems relating to IT. 

The Books is a management software EIS created for a new private library in Danang. It is a modern, modular library management system that supports the workflows of library staff for both web-based management and mobile application. 


“This is the best IT company I’ve ever worked with. Young, energetic, enthusiastic. You’ve turned The Books project into your own project, exploring solutions for The Books like that is your own project. I’m glad to work with Enouvo!” – Nguyen Thai Nhat Nguyen – Founder of The Books Library & Coffee 

![enouvo](https://ucarecdn.com/deded3fb-ac0d-4784-8b7e-3add555888cf/)

Until now, Enouvo IT Solutions has launched many projects of international partners from Australia, USA, UK, Singapore and so on. In which, My accountant is a mobile tax app, complying with the Australia Tax Office obligation. Reebonz is a trusted online platform for buying and selling the widest range of luxury products across the world. Personnel IT, Cloud loT were used in the USA.

By cooperating and creating useful and purposeful solutions for international partners, EIS hopes to contributing to increasing the quality of life.Developing the co-working space

![enouvo](https://ucarecdn.com/0f484582-7343-4c31-bce0-0ff3749e1ec5/)

## Developing the co-working space

Having a big love for Da Nang, Trang always wants to come back to her hometown to contribute to its development. In 2015, Trang and her husband left Melbourne and continued developing their career path in Da Nang city.

Da Nang provides a better balance between work and life, with the beach and the mountains nearby. But there are not enough places where people can be part of a community and experience real synergy. That’s the reason why Trang wants to develop the model of co-working space in this city. In 2017, Enouvo Space was born, which has become a familiar destination of freelancers, digital nomads, and expats when coming to Da Nang. After two years, Enouvo Space 2 was launched and became the first co-working and co-living eco-system in this central city.

“Enouvo Space offers daily or monthly co-working space, studios, and co-living space. It’s the place to work but also to connect and share ideas. With Enouvo Space, we hope to promote, introduce not only Da Nang but also Vietnam to the world as an ideal destination for startups and businesses. By creating an ideal business environment, we can attract the talents return to Da Nang and Vietnam. It is necessary to create a new ecosystem, an ideal environment, and provide optimized support for them to connect, work together and create outstanding products.” – Trang shared.
