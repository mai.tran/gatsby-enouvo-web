import React from "react"
import PropTypes from "prop-types"
// import { BlogPostTemplate } from "../../templates/blog-post"

const BlogPostPreview = ({ entry, widgetFor }) => {
  console.log("entry: ", entry)
  const body = widgetFor('body');
  const title = entry.getIn(['data', 'title']);
  return (
    // <BlogPostTemplate
    //   content={widgetFor('body')}
    //   description={entry.getIn(['data', 'description'])}
    //   tags={entry.getIn(['data', 'tags'])}
    //   title={entry.getIn(['data', 'title'])}
    // />
    // <div>đoodododododohghghsidmsdfisdfod</div>
    <div className="page">
      <h1 className="page__title">{title}</h1>
      <div className="page__body">{body}</div>
    </div>
  )
}
BlogPostPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default BlogPostPreview
