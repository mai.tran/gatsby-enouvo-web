import React from 'react';
import PropTypes from 'prop-types';
import { Carousel, Row, Col } from 'antd';
import { BannerWrapper, Image } from './styles';

const Banner = ({ data }) => {
  return (
    <BannerWrapper>
      <div className="round_shapb" />
      <Carousel autoplay>
        {data.map((item, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <div className="banners-items" key={index}>
            <Row type="flex" justify="space-between">
              <Col md={12} xs={24} sm={24} className="banners-details">
                <h1>{item.title}</h1>
                <p>{item.description}</p>
              </Col>
              <Col md={12} xs={24} sm={24} className="banners-image">
                <Image src={item.image} />
              </Col>
            </Row>
          </div>
        ))}
      </Carousel>
      <div className="round_shapbottom" />
    </BannerWrapper>
  );
};

Banner.propTypes = {
  data: PropTypes.any,
};

Banner.defaultProps = {
  data: [
    {
      title: 'Connect the physical and the digital world',
      banner:
        'https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570418960/BLOG/enouvo-gatsby/banner_ui8g7d.png',
      description:
        "Technological products simplify people's lives with optimized solutions.",
    },
    {
      title: 'Connect the physical and the digital world',
      banner:
        'https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570418960/BLOG/enouvo-gatsby/banner_ui8g7d.png',
      description:
        "Technological products simplify people's lives with optimized solutions.",
    },
    {
      title: 'Connect the physical and the digital world',
      banner:
        'https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570418960/BLOG/enouvo-gatsby/banner_ui8g7d.png',
      description:
        "Technological products simplify people's lives with optimized solutions.",
    },
  ],
};

export default Banner;
