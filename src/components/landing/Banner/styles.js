import styled from 'styled-components';

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat;
  background-size: cover;
  border-radius: 50%;
  width: 23rem;
  height: 23rem;
  position: relative;
  @media (min-width: 320px) {
    width: 17rem;
    height: 17rem;
  }
  @media (min-width: 576px) {
    width: 17rem;
    height: 17rem;
  }
  @media (min-width: 768px) {
    width: 20rem;
    height: 20rem;
  }
  @media (min-width: 992px) {
    width: 28.125rem;
    height: 28.125rem;
  }
  &::before {
    content: '';
    position: absolute;
    top: -4%;
    left: 4%;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    background-color: #c1351f;
    z-index: -10;
  }
  &::after {
    content: '';
    position: absolute;
    top: 4%;
    left: -4%;
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: solid 1.5px #c1351f;
    z-index: -10;
  }
`;

export const BannerWrapper = styled.div`
  .ant-carousel {
    .slick-slide {
      text-align: center;
      line-height: 160px;
      overflow: hidden;
      z-index: 100;
      h3 {
        color: #fff;
      }
    }
  } 

  .banners-details {
    text-align: left;
    align-self: center;
    p {
      font-size: 1rem;
      line-height: 1.6;
      letter-spacing: 0.27px;
      color: #3a393d;
      font-family: Poppins;
    }
  }

  .banners-items {
    /* padding-top: 10rem;
    padding-left: 7%;
    padding-right: 7%;
    padding-bottom: 5rem; */
    margin-bottom: 20px;
    @media (max-width: 576px) {
      padding-top: 7rem;
    }
    .ant-row-flex {
      margin: 1rem 0rem;
      @media (min-width: 576px)  { 
        margin: 8rem 0rem 1rem 0rem;
      }
      @media (min-width: 768px) {
        .banners-details {
          p {
            font-size: 12px;
            line-height: 1.6;
            letter-spacing: 0.27px;
            color: #3a393d;
            font-family: Poppins;
          }
        }
      }
      @media (min-width: 992px)  { 
        margin: 11.6rem 0rem 0 0rem;
        .banners-details {
          p {
            font-size: 16px;
            line-height: 1.6;
            letter-spacing: 0.27px;
            color: #3a393d;
            font-family: Poppins;
          }
        }
      }
      @media (min-width: 1200px)  { 
        .banners-details {
          p {
            font-size: 16px;
            line-height: 1.6;
            letter-spacing: 0.27px;
            color: #3a393d;
            font-family: Poppins;
          }
        }
      }
      
    }
  }
  .ant-carousel {
      .slick-dots {
        li {
          margin: 0px 2px;
          width: 14px;
          height: 14px;
          border-radius: 50%;
          background: rgb(216, 216, 216);
          margin-right: 16px;
          button {
            background: transparent;
            &::before {
              font-size: 12px;
            }
          }
        }
        .slick-active {
          background: rgb(193, 53, 31);
          opacity: 0.5;
          button {
            background: transparent;
            &::before {
              color: rgba(193, 53, 31, 0.5);
            }
          }
        }
      }
    }
  } 

  img {
    width: 100%;
  }
  .banners-image {
    display: flex;
    justify-content: center;
  }
  .banners-details {
    h1 {
      text-transform: uppercase;
      font-size: 2.5rem;
      line-height: 1.5;
      font-style: normal;
      font-weight: bold;
      letter-spacing: 0.53px;
      font-family: Poppins-Bold;
      font-weight: bold;
      &::before {
        content: '';
        height: 80px;
        width: 80px;
        position: absolute;
        background: #c1351f;
        right: 5%;
        z-index: -1;
        border-radius: 50%;
        opacity: 0.7;
        top: -50%;
      }
      span {
        text-shadow: -1px -1px 0 #c13520, 1px -1px 0 #c13520, -1px 1px 0 #c13520, 1px 1px 0 #c13520;
        color: #fff;
        text-transform: uppercase;
        padding-right: 1.625rem;
        letter-spacing: 0.375rem;
        line-height: 1.46;
      }
    }
  } 
  .round_shapb {
    position: absolute;
    top: 245px;
    left: -50px;
    border-radius: 50%;
    width: 150px;
    height: 150px;
    background: #c1351f;
  }

  .round_shapbottom {
    position: absolute;
    top: 550px;
    left: 130px;
    border: 1px solid #c13520;
    border-radius: 50%;
    width: 80px;
    height: 80px;
  }

  .round_shapright {
    position: absolute;
    top: 493px;
    right: -2%;
    opacity: 0.7;
    border-radius: 50%;
    width: 150px;
    height: 150px;
    background: #c1351f;

    & :before {
      content: '';
      height: 50px;
      width: 50px;
      position: absolute;
      left: -50%;
      z-index: -1;
      border-radius: 50%;
      border: 1px solid #c13520;
      animation: moveRoundbRight 10s linear 2s infinite alternate;
      top: 0;
    }
  }

  @keyframes moveShap2 {
    0% {
      right: 230px;
      top: 85px;
    }

    25% {
      right: 100px;
      top: 85px;
    }

    50% {
      right: 100px;
      top: 100px;
    }

    75% {
      right: 230px;
      top: 200px;
    }

    100% {
      right: 220px;
      top: 100px;
    }
  }

  @keyframes hue {
    from {
      border: 2px solid #c13520;
    }

    to {
      border: 2px solid #fad0d0;
    }
  }

  @keyframes moveRoundb {
    0% {
      background-color: rgb(193, 53, 31);
      right: 20%;
      top: -50%;
    }

    25% {
      background-color: rgb(193, 53, 31);
      right: 50%;
      top: -35%;
    }

    50% {
      background-color: rgb(193, 53, 31);
      right: 30%;
      top: -50%;
    }

    75% {
      background-color: rgb(193, 53, 31);
      right: 40%;
      top: -45%;
    }

    100% {
      background-color: rgb(193, 53, 31);
      right: 40%;
      top: -50%;
    }
  }

  @keyframes moveRoundbRight {
    0% {
      left: -50%;
      top: 0;
    }

    25% {
      left: -40%;
      top: 15%;
    }

    50% {
      left: -20%;
      top: 5%;
    }

    75% {
      left: -40%;
      top: -10%;
    }

    100% {
      left: -50%;
      top: 0;
    }
  }

  @keyframes moveShapleft {
    0% {
      top: 60%;
      left:100px;
    }

    25% {
      top: 55%;
      left: 120px;
    }

    50% {
      top: 45%;
      left: 150px;
    }

    75% {
      top: 40%;
      left: 120px;
    }

    100% {
      top: 60%;
      left: 100px;
    }
  }

  @keyframes moveShapright {
    0% {
      background-color: rgb(193, 53, 31);
      top: 500px;
      right: -5%;
    }

    25% {
      background-color: rgb(193, 53, 31);
      top: 420px;
      right: 5%;
    }

    50% {
      background-color: rgb(193, 53, 31);
      top: 470px;
      right: 0;
    }

    75% {
      background-color: rgb(193, 53, 31);
      top: 430px;
      right: 5%;
    }

    100% {
      background-color: rgb(193, 53, 31);
      top: 500px;
      right: -5%;
    }
  }

    25% {
      background-color: rgb(193, 53, 31);
      right: 50%;
      top: 0px;
    }

    50% {
      background-color: rgb(193, 53, 31);
      right: 50%;
      top: 100px;
    }

    75% {
      background-color: rgb(193, 53, 31);
      right: 40%;
      top: 200px;
    }

    100% {
      background-color: rgb(193, 53, 31);
      right: 40%;
      top: 0px;
    }
  }

  @media (max-width: 992px) {
    .round_shapright {
      display: none;
    }
  }
  @media (max-width: 576px) {
    .banners-details {
      padding-bottom: 2rem;
      h1 {
        font-size: 2.5rem;
      }
    }
  }
`;
