import styled from 'styled-components';

export const BusinessWrapper = styled.div`
         padding-top: 5rem;
         ul {
           padding-left: 0;
           li {
             list-style: none;
           }
         }
         .business-items {
           padding-left: 7%;
           padding-right: 7%;
           padding-bottom: 4rem;
         }

         .business-image {
           img {
             width: 80%;
             margin: auto;
           }
         }

         .business-details {
           padding-left: 2rem;
           padding-right: 2rem;
           align-self: center;
           h2 {
             width: min-intrinsic;
             width: -webkit-min-content;
             width: -moz-min-content;
             width: min-content;
             letter-spacing: 10px;
             text-shadow: -1px -1px 0 #c13520, 1px -1px 0 #c13520,
               -1px 1px 0 #c13520, 1px 1px 0 #c13520;
             color: #fff;
             text-transform: uppercase;
             font-size: 2.8rem;
           }
           h3 {
             color: inherit !important;
             padding-top: 1rem;
             padding-bottom: 1rem;
             font-size: 1.4rem;
           }
         }
         .business-details-list {
           padding-bottom: 2rem;
           li {
             background-image: url('https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570415921/BLOG/enouvo-gatsby/bullet_ulurl8.png');
             padding: 0em 0px 0.5em 20px;
             background-repeat: no-repeat;
             background-position: left center;
             background-size: 10px;
             background-position-y: 6px;
           }
         }

         .ant-carousel {
           .slick-dots {
             li {
               margin: 0px 2px;
               width: 14px;
               height: 14px;
               border-radius: 50%;
               background: rgb(216, 216, 216);
               margin-right: 16px;
               button {
                 background: transparent;
                 &::before {
                   font-size: 12px;
                 }
               }
             }
             .slick-active {
               background: rgb(193, 53, 31);
               opacity: 0.5;
               button {
                 background: transparent;
                 &::before {
                   color: rgba(193, 53, 31, 0.5);
                 }
               }
             }
           }
         }

         button.slick-arrow.slick-prev {
           margin-left: 5%;
           left: 0px;
           width: 50px;
           height: 50px;
           background: transparent;
           border-radius: 53%;
           border: 1px solid #c13520;
           z-index: 9;
           & :hover:before {
             background-image: url('https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570419996/BLOG/enouvo-gatsby/lefthover_pp6t3q.png');
           }
         }

         .ant-carousel {
           .slick-prev::before {
             background-image: url('https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570419996/BLOG/enouvo-gatsby/left_xb7uiq.png');
             background-repeat: no-repeat;
             background-size: 27px;
             width: 50px;
             height: 50px;
             background-position-x: 0px;
             color: transparent;
             line-height: 72px;
             font-size: 2.1rem;
             opacity: 1;
           }
         }

         button.slick-arrow.slick-next {
           margin-right: 5%;
           right: 0px;
           width: 50px;
           height: 50px;
           background: transparent;
           border-radius: 53%;
           border: 1px solid #c13520;
           z-index: 9;
           & :hover:before {
             background-image: url('https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570419997/BLOG/enouvo-gatsby/righthover_xwfvbm.png');
           }
           &::before {
             background-image: url('https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570419997/BLOG/enouvo-gatsby/right_v4618d.png');
             background-repeat: no-repeat;
             background-size: 27px;
             width: 50px;
             height: 50px;
             background-position-x: 0px;
             color: transparent;
             line-height: 72px;
             font-size: 2.1rem;
             opacity: 1;
           }
         }

         button.slick-arrow:hover {
           background: #c13520;
         }

         .ant-carousel {
           .slick-slide {
             h3 {
               color: #fff;
             }
           }
         }

         img {
           width: 100%;
         }

         @media (max-width: 768px) {
           .business-details {
             padding-left: 1rem;
             h2 {
               width: auto;
               font-size: 2rem;
               line-height: 2.5rem;
               padding-bottom: 0.5rem;
             }
           }
           .business-image {
             align-self: center;
           }
         }

         @media (max-width: 576px) {
           .slick-arrow {
             display: none !important;
           }

           .business-details {
             h2 {
               padding-top: 2rem;
               width: auto;
               font-size: 2.5rem;
               text-align: center;
             }
             p {
               text-align: center;
             }
             h3 {
               color: inherit !important;
               text-align: center;
               padding-top: 1rem;
               padding-bottom: 0.5rem;
               font-size: 1.2rem;
             }
           }

           .business-details-list {
             padding-left: 15%;
             padding-right: 10%;
           }

           .buttonCommon {
             text-align: center;
           }

           .businessButton {
             text-align: center;
           }
         }
       `;
