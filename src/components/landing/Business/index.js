/* eslint-disable no-shadow */
import React from 'react';
import PropTypes from 'prop-types';
import { Carousel, Row, Col } from 'antd';
import { BusinessWrapper } from './styles';
import ButtonCommon from '../../common/ButtonCommon';

const Business = () => {
  const DATA = {
    business: [
      {
        title: 'Business Project',
        image:
          'https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570418961/BLOG/enouvo-gatsby/businessProject_d7kcea.png',
        description:
          'A high-quality solution that helps your business save time and fit your budget with continuous support.',
        subTitle: 'We provide',
        checkList: [
          {
            listTitle: ' Agile-focused development teams',
          },
          {
            listTitle: 'Time and tech savings',
          },
          {
            listTitle: 'Full cycle product development',
          },
          {
            listTitle: 'Supporting Digital Marketing',
          },
        ],
        button: {
          titleButton: 'View Case Study',
          linkButton: '#',
        },
      },
      {
        title: 'Business Project',
        image:
          'https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570418961/BLOG/enouvo-gatsby/businessProject_d7kcea.png',
        description:
          'A high-quality solution that helps your business save time and fit your budget with continuous support.',
        subTitle: 'We provide',
        checkList: [
          {
            listTitle: ' Agile-focused development teams',
          },
          {
            listTitle: 'Time and tech savings',
          },
          {
            listTitle: 'Full cycle product development',
          },
          {
            listTitle: 'Supporting Digital Marketing',
          },
        ],
        button: {
          titleButton: 'View Case Study',
          linkButton: '#',
        },
      },
      {
        title: 'Business Project',
        image:
          'https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570418961/BLOG/enouvo-gatsby/businessProject_d7kcea.png',
        description:
          'A high-quality solution that helps your business save time and fit your budget with continuous support.',
        subTitle: 'We provide',
        checkList: [
          {
            listTitle: ' Agile-focused development teams',
          },
          {
            listTitle: 'Time and tech savings',
          },
          {
            listTitle: 'Full cycle product development',
          },
          {
            listTitle: 'Supporting Digital Marketing',
          },
        ],
        button: {
          titleButton: 'View Case Study',
          linkButton: '#',
        },
      },
    ],
  };

  return (
    <BusinessWrapper className="business">
      <Carousel arrows autoplay>
        {DATA.business.map((item, index) => {
          return (
            // eslint-disable-next-line react/no-array-index-key
            <div className="business-items" key={index}>
              <Row type="flex">
                <Col sm={12} xs={24} className="business-image">
                  <img src={item.image} alt="" />
                </Col>
                <Col sm={12} xs={24} className="business-details">
                  <h2>{item.title}</h2>
                  <p>{item.description}</p>
                  <h3>{item.subTitle}</h3>
                  <ul className="business-details-list">
                    {item.checkList.map((list, index) => {
                      // eslint-disable-next-line react/no-array-index-key
                      return <li key={index}>{list.listTitle}</li>;
                    })}
                  </ul>
                  <div className="businessButton">
                    <ButtonCommon item={item} />
                  </div>
                </Col>
              </Row>
            </div>
          );
        })}
      </Carousel>
    </BusinessWrapper>
  );
};

Business.propTypes = {
  // eslint-disable-next-line react/no-unused-prop-types
  data: PropTypes.any,
};

export default Business;
