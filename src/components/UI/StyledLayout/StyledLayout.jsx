import React from 'react';
import PropTypes from 'prop-types';
import { StyledLayoutWrapper } from './StyledLayout.style';

const StyledLayout = ({
  children, xs, sm, md, lg, xl
}) => {
  return (
    <StyledLayoutWrapper
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...{
        xs,
        sm,
        md,
        lg,
        xl,
      }}
    >
      {children}
    </StyledLayoutWrapper>
  );
};

StyledLayout.propTypes = {
  children: PropTypes.node.isRequired,
  xs: PropTypes.string,
  sm: PropTypes.string,
  md: PropTypes.string,
  lg: PropTypes.string,
  xl: PropTypes.string,
};

StyledLayout.defaultProps = {
  xs: '1rem',
  sm: '1rem',
  md: '5rem',
  lg: '4.4795rem',
  xl: '7.0625rem',
};

export default StyledLayout;
