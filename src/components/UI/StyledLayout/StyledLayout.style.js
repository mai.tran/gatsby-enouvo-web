import styled from 'styled-components';

export const StyledLayoutWrapper = styled.div`
  margin: 0 ${props => props.xs};
  @media (min-width: 576px) {
    margin: 0 ${props => props.sm};
  }
  @media (min-width: 768px) {
    margin: 0 ${props => props.md};
  }
  @media (min-width: 992px) {
    margin: 0 ${props => props.lg};
  }
  @media (min-width: 1200px) {
    margin: 0 ${props => props.xl};
  }
`;
