import styled from 'styled-components';

export const StyledFlexBox = styled.div`
  display: flex;
  flex-direction: ${props => props.flexDirection || 'row'};
  justify-content: ${props => props.justifyContent || 'flex-start'};
  flex-wrap: ${props => props.flexWrap || 'no-wrap'};
  align-items: ${props => props.alignItems || 'flex-start'};
  margin: ${props => props.margin / 16 || 0}rem;
  padding-top: ${props => props.paddingTop / 16 || 0}rem;
  padding-left: ${props => props.paddingLeft / 16 || 0}rem;
  padding-bottom: ${props => props.paddingBottom / 16 || 0}rem;
  padding-right: ${props => props.paddingRight / 16 || 0}rem;
  margin-top: ${props => props.marginTop / 16 || 0}rem;
  margin-left: ${props => props.marginLeft / 16 || 0}rem;
  margin-bottom: ${props => props.marginBottom / 16 || 0}rem;
  margin-right: ${props => props.marginRight / 16 || 0}rem;
`;
