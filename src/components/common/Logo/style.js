import styled from 'styled-components';

export default styled.img`
  height: 74px;
  width: 240px;
  object-fit: contain;
  @media (max-width: 991px) {
    width: 150px;
    margin-left: 10%;
  }
  @media (max-width: 576px) {
    margin-left: 0;
  }
`;
