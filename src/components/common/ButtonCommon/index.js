import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '@reach/router';
import { ButtonCommonWrapper } from './styles';

const ButtonCommon = ({ item }) => (
  <ButtonCommonWrapper>
    <Link className="buttonCommon" to={item.button.linkButton}>
      {item.button.titleButton}
    </Link>
  </ButtonCommonWrapper>
);
ButtonCommon.propTypes = {
  item: PropTypes.object,
};

export default ButtonCommon;
