import styled from 'styled-components';

export const ButtonCommonWrapper = styled.div`
  .buttonCommon {
    background: #c1351f;
    color: #fff;
    position: relative;
    padding: 0.8rem 2rem;
    border: 1px solid #c1351f;
    border-radius: 0.1rem;
    & :hover {
      background: #fff;
      color: #c13520;
      border: 1px solid #c13520;
    }
  }
`;
