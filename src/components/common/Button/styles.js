import styled from 'styled-components';

export const ButtonWrapper = styled.a`
  background: ${({ theme }) => theme.primary[0]};
  &:hover {
    background: ${({ theme }) => theme.primary[0]};
    box-shadow: 0px 25px 42px 0px ${({ theme }) => theme.shadow[0]};
  }
`;
