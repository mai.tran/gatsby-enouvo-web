/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import Subscribe from '../Subscribe';
import Header from '../Header';
// import ButtonScroll from '../ButtonScroll';
import { MainWrapper, LayoutStyled, GlobalStyle } from './styles';
import theme from '../../../config/lightTheme';
// import '../../../i18n';

export const Layout = ({ children, version }) => {
  console.log("children, version", children, version);

  return (
    <ThemeProvider theme={theme}>
      <MainWrapper>
        <GlobalStyle />
        <Header />
        <LayoutStyled>
          {children}
          {/* <Subscribe /> */}
        </LayoutStyled>
        {/* <Footer version={version} /> */}
        {/* <ButtonScroll /> */}
      </MainWrapper>
    </ThemeProvider>
  );
};

Layout.propTypes = {
  children: PropTypes.any,
  version: PropTypes.number,
};

Layout.defaultProps = {
  version: 1,
};

export default Layout;
