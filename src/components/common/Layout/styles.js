import styled, { createGlobalStyle } from 'styled-components';

export const StyledLayout = styled.div`
  margin: 0 1rem;
  @media (min-width: 768px) {
    margin: 0 5rem;
  }
  @media (min-width: 992px) {
    margin: 0 4.4795rem;
  }
  @media (min-width: 1200px) {
    margin: 0 7.0625rem;
  }
`;

export const LayoutStyled = styled.div`
  width: 100%;
  ${({ theme }) => theme === 'dark' && 'background: #212121;'};
`;

export const GlobalStyle = createGlobalStyle`
@font-face {
    font-family: Poppins;
    src: url(/fonts/Poppins-Regular.ttf);
  }

  @font-face {
    font-family: Poppins-Medium;
    src: url(/fonts/Poppins-Medium.ttf);
  }

  @font-face {
    font-family: Poppins-SemiBold;
    src: url(/fonts/Poppins-SemiBold.ttf);
  }

  @font-face {
    font-family: Poppins-Bold;
    src: url(/fonts/Poppins-Bold.ttf);
  }
  html,
  body {
  }
`;

export const MainWrapper = styled.div`
  scroll-behavior: smooth;
  overflow: hidden;
  body {
    font-family: 'Poppins', sans-serif;
    scroll-behavior: smooth;
    overflow-x: hidden;
  }
  .responsiveVideo {
    position: relative;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;

    iframe,
    object,
    embed {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
  }

  a {
    text-decoration: none;
  }

  button,
  input,
  select,
  textarea:focus {
    outline: none;
  }

  .shadow {
    box-shadow: 0 36px 64px 0 ${({ theme }) => theme.shadow[0]};
  }
  .shadow-light {
    box-shadow: 0 2px 40px 1px ${({ theme }) => theme.shadow[0]};
    transition: all 0.2s ease-in-out;
  }

  .shadow-light:hover {
    transform: scale(1.03);
  }

  .shadow-super-light {
    display: none;
    box-shadow: 0 5px 40px 0px ${({ theme }) => theme.shadow[1]};
    transition: all 0.2s ease-in-out;
  }

  .background-gradient {
    background: #00c3ff;
    /* background-image: linear-gradient(90deg,#f7962c,#f26122)!important; */
  }
  .theme_btn {
    pointer-events: auto;
    background: ${({ theme }) => theme.primary[0]};
    box-shadow: 0px 20px 35px 1px ${({ theme }) => theme.primary[0]}35 !important;
    border-radius: 4px;
    transition: all 0.2s ease-in-out;
    font-family: 'Poppins-Medium' !important;
    ${'' /* font-size: 20px !important; */}
    &:hover {
      transform: scale(1.05) translateY(0);
      box-shadow: 0px 25px 42px 0px ${({ theme }) => theme.primary[0]}35 !important;
    }
  }
  h4 {
    color: ${({ theme }) => theme.primary[0]};
    font-family: 'Poppins-Medium';
    font-size: 20px;
    padding-bottom: 20px;
  }
  h2 {
    color: ${({ theme }) => theme.text[0]};
    font-family: 'Poppins-SemiBold';
    font-size: 48px;
    line-height: 52px;
  }
`;
