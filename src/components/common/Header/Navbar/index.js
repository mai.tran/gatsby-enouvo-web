import React from 'react';
import { Link } from 'gatsby';
import { Container, Logo } from '../..';
import NavbarLinks from '../NavbarLinks';
import { Wrapper, BrandLogo, Brand } from './styles';

const Navbar = () => (
      <Wrapper as={Container}>
        <Brand as={Link}  to="/">
          <BrandLogo
            as={Logo}
            color={theme === 'dark' ? '#fff' : '#212121'}
            strokeWidth="2"
          />
          doananh234
        </Brand>
        <NavbarLinks desktop />
      </Wrapper>
);

export default Navbar;
