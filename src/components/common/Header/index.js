import React from 'react';
// eslint-disable-next-line import/no-cycle
import { Link } from '@reach/router';
import { StyledHeader } from './styles';
import NavItem from './NavItem';
import RightNav from './RightNav';
import { Logo } from '../Logo';

const HeaderWrapper = () => {
  const NAV = [
    {
      title: 'Home',
      href: '/',
    },
    {
      title: 'Service',
      href: '/service',
    },
    {
      title: 'Company',
      href: '/company',
    },
    {
      title: 'Portfolilios',
      href: '/portfolilios'
    },

    {
      title: 'Blog',
      href: '/blog',
    },
    {
      title: 'Careers',
      href: '/careers',
    },
    // {
    //   title: 'Gallery',
    //   href: '/gallery',
    // },
    // {
    //   title: 'Project',
    //   href: '/project',
    // },
  ];
  return (
    <StyledHeader>
      <header className="main_header_area">
        <nav className="navbar navbar-expand-lg">
          <div className="navbar-logo">
            <Link className="navbar-brand" to="/">
              <Logo />
            </Link>
          </div>
          {/* <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target=".navbar_supported"
            aria-label="Toggle navigation"
          >
            <i className="fa fa-bars" aria-hidden="true" />
          </button> */}
          <div className="collapse navbar-collapse navbar_supported">
            <ul className="navbar-nav">
              {NAV.map((item, index) => (
                <NavItem key={String(index)} item={item} />
              ))}
            </ul>
          </div>
          <RightNav />
        </nav>
      </header>
    </StyledHeader>
  );
};

HeaderWrapper.propTypes = {};

export default HeaderWrapper;
