import React from 'react';
import PropTypes from 'prop-types';
import { HamburgerIcon, Bar } from './styles';

const Hamburger = ({ sidebar, toggle, isHomePage }) => (
  <HamburgerIcon isHomePage={isHomePage} sidebar={sidebar} onClick={toggle}>
    <Bar top sidebar={sidebar} />
    <Bar mid sidebar={sidebar} />
    <Bar bottom sidebar={sidebar} />
  </HamburgerIcon>
);

Hamburger.propTypes = {
  sidebar: PropTypes.any,
  toggle: PropTypes.bool,
  isHomePage: PropTypes.bool,
};

export default Hamburger;
