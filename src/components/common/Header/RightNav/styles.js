import styled from 'styled-components';

export const RightNavWrapper = styled.ul`
  .theme_btn {
    background: ${({ theme }) => theme.primary[0]};
    ${'' /* height: 45px; */}
    ${'' /* width: 150px; */}
    border-radius: 2px;
    border: solid 2px #c1351f;
    font-family: 'Poppins-Medium' !important;
    ${'' /* font-size: 20px !important; */}
    font-weight: 500 !important;
    &:hover {
      box-shadow: 0px 25px 42px 0px ${({ theme }) => `${theme.primary[0]}35`};
    }
  }
`;
