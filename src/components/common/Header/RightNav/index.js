import React from 'react';
import I18n from 'i18next';
import { RightNavWrapper } from './styles';
import Button from '../../Button';

const RightNav = () => {
  return (
    <RightNavWrapper className="right_nav">
      <li>
        <Button href="#contact" className="theme_btn">
          {I18n.t('button.contactUs')}
        </Button>
      </li>
    </RightNavWrapper>
  );
};
RightNav.propTypes = {};

export default RightNav;
