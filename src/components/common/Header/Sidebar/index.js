import React from 'react';
import PropTypes from 'prop-types';
import NavbarLinks from '../NavbarLinks';
import { SidebarContainer } from './styles';

const Sidebar = ({ sidebar, toggle }) => (
  <SidebarContainer active={sidebar} onClick={toggle} theme={theme}>
    <NavbarLinks />
  </SidebarContainer>
);

Sidebar.propTypes = {
  sidebar: PropTypes.any,
  toggle: PropTypes.func,
};

export default Sidebar;
