import styled from 'styled-components';

export const StyledHeader = styled.div`
  width: 100%;
  z-index: 1;
  margin-bottom: unset;
  background: ${({ theme }) => theme.background.content};
  box-shadow: 0px 2px 20px 0 ${({ theme }) => theme.shadow[1]};
  
  .main_header_area {
    display: block;
    width: 100%;
    z-index: 9999;
    position: absolute;
    padding: 2.375rem 7.0625rem 0;
    &.navbar_fixed {
      position: fixed;
      width: 100%;
      top: -70px;
      left: 0;
      right: 0;
      z-index: 99999;
      padding: 5px 200px;
      background: #fff;
      box-shadow: 0px 0px 18px 0px rgba(222, 222, 222, 0.75);
      transform: translateY(70px);
      transition: transform 500ms ease, background 200ms ease;
    }
  }
  .navbar {
    padding: 0;
    ${'' /* width: 89%; */}
    @media (max-width: 1440px) {
      ${'' /* width: 93%; */}
    }
    display: flex;
    align-items: center;
    .navbar-logo {
      flex: 1;
    }
    .right_nav {
      float: right;
      padding-left: 12.5px;
      margin-bottom: 0px;
      li {
        display: inline-block;
        float: left;
        a {
          width: 150px;
          text-align: center;
          margin: 0;
          color: #282331;
          font: 400 16px/42px "Niramit", sans-serif;
          position: relative;
          display: block;
          i:before {
            font-size: 16px;
            line-height: 50px;
          }
          &:hover , &:focus {
            color: #8f49f9;
          }
        }
        a.theme_btn , a.theme_btn:hover, a.theme_btn:focus {
          background: #fff;
          color : #c1351f;
          border: 2px solid #c1351f;
          border-radius: 2px;
          box-shadow: 0px 20px 35px 1px #fff !important;
        }
      }
      li.dropdown {
        position: relative;
        .dropdown-menu {
          margin: 0;
          padding: 30px;
          border-radius: 10px;
          box-shadow: 0px 0px 18px 0px rgba(222, 222, 222, 0.75);
          &::before {
            content: "\f0d8";
            font: normal normal normal 30px/1 FontAwesome;
            position: absolute;
            top: -17px;
            left: 25px;
            color: #fff;
          }
        }
      }
    }
    .navbar-brand {
      line-height: 50px;
      padding: 0;
      min-width: 50px;
    }
  }
  .navbar_supported {
    float: right;
    .navbar-nav {
      width: 100%;
      display: flex;
      @media (min-width: 992px)  { 
        margin: 0;
        padding: 0;
      }
      li {
        list-style: none;
        position: relative;
        a {
          padding: 0;
          color: #282331;
          font: 400 16px/50px "Niramit", sans-serif;
          position: relative;
          display: block;
          z-index: 1;
          @media (max-width: 992px)  { 
           }
          @media (max-width: 1200px) { 
          }
          @media (max-width: 1600px) { 
          }
          &::after {
            content: '';
            position: absolute;
            left: 0;
            width: 0%;
            background: url("https://res.cloudinary.com/coders-tokyo-shyn/image/upload/v1570419997/BLOG/enouvo-gatsby/menu-shap_jxcas9.png") no-repeat;
            height: 13px;
            bottom: 0;
            border: 0;
            margin: 0;
          }
          &:hover, &:focus, &.active {
            color: #8f49f9;
            &::after {
              width: 100%;
            }
          }
        }
        a.theme_btn {
          margin-top: 2px !important;
        }
      }
      li.dropdown {
        position: relative;
        .dropdown-menu {
          margin: 0;
          padding: 10px 0;
          border-radius: 10px;
          box-shadow: 0px 0px 18px 0px rgba(222, 222, 222, 0.75);
          &::before {
            content: "\f0d8";
            font: normal normal normal 30px/1 FontAwesome;
            position: absolute;
            top: -17px;
            left: 25px;
            color: #fff;
          }
        }
      }
    }
  }

.searchForm {
  height: 0;
  background: #fff;
  box-shadow: 0px 0px 18px 0px rgba(222, 222, 222, 0.75);
  overflow: hidden;
  transition: all 300ms linear 0s;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  padding: 0 200px;
  z-index: 99999999;
  .input-group-addon {
    border-radius: 0;
    border: none;
    font-size: 24px;
    padding: 0 40px;
    background: transparent;
    color: #282331;
    cursor: pointer;
    line-height: 70px;
    i:before {
      font-size: 24px;
    }
    &:hover, &:focus {
      color: #8f49f9;
    }
  }
  .form-control {
    height: auto;
    padding: 0 15px;
    border-radius: 0;
    border: none;
    background: transparent;
    text-align: center;
    font: 400 18px/70px "Niramit", sans-serif;
  }
  &.show {
    height: 70px;
  }
}

@media (max-width: 991px) {
  .navbar {
    .navbar_supported {
      ul {
        li.dropdown {
          .dropdown-menu {
            position: absolute;
            top: 100%;
            left: -40px;
            min-width: 240px;
            background-color: #fff;
            opacity: 0;
            transition: all 300ms ease-in;
            visibility: hidden;
            display: block;
            border: none;
            a {
              background-color: transparent;
              font: 400 18px/40px "Niramit", sans-serif;
              color: rgba(40, 35, 49, 0.702);
              margin: 0;
              display: block;
              padding: 0 25px;
              position: relative;
              cursor: pointer;
              &::after {
                display: none;
              }
              &::before {
                content: "";
                position: absolute;
                left: 16px;
                top: 18px;
                border: 0;
                width: 5px;
                height: 5px;
                background: #ff8d68;
                border-radius: 50%;
                opacity: 0;
              }
              &:hover, &:focus, &.active {
                color: #ff8d68;
                padding-left: 30px;
                &::before {
                  opacity: 1;
                }
              }
            }
            .dropdown {
              position: relative;
              a:after {
                border: 0;
              }
              &::after {
                content: "\f105";
                position: absolute;
                top: 0;
                right: 20px;
                font: normal normal normal 15px/48px FontAwesome;
                color: #8f49f9;
              }
              .dropdown-menu {
                transform: translateZ(0);
                transform: scaleX(0);
                transform-origin: 0 50%;
                position: absolute;
                left: 100%;
                right: auto;
                width: auto;
                top: 0px;
                &::before {
                  content: "\f0d9";
                  font: normal normal normal 30px/1 FontAwesome;
                  color: rgba(0, 0, 0, 0.05);
                  top: 7px;
                  left: -10px;
                }
              }
              &:hover , &:focus {
                transform: scale(1, 1);
              }
            }
          }
          &:hover , &:focus {
            .dropdown-menu {
              left: 0px;
              visibility: visible;
              opacity: 1;
            }
          }
        } 
      } 
    } 
    .right_nav {
      li {
        .theme_btn {
          ${'' /* padding: 0 30px; */}
          margin-right: 0;
          margin-left: 0;
          color: #fff;
          text-transform: inherit;
          text-align: center !important;
          &:hover, &:focus {
            color: #fff;
          }
        }
        .sign_in_2 {
          color: #b49ffa;
          background: transparent;
          border: 1px solid #ece7fe;
          &:hover, &:focus {
            color: #fff;
            background: #8f49f9;
          }
        }
      }
      li.dropdown:hover, li.dropdown:focus {
        .dropdown-menu {
          left: 0px;
          visibility: visible;
          opacity: 1;
        }
      }
    }
  }
}

@media (max-width: 1440px) {
  .main_header_area {
    padding-left: 80px;
    padding-right: 80px;
  }

  .main_header_area.navbar_fixed {
    padding-left: 80px;
    padding-right: 80px;
  }
}
@media (max-width: 1280px) {
  .main_header_area {
    padding-left: 7.18rem;
    padding-right: 60px;
  }
}

@media (max-width: 1199px) {
  .main_header_area {
    padding: 2.375rem 4.4795rem 0;
  }
  .main_header_area.navbar_fixed {
    padding-left: 4.4795rem;
    padding-right: 15px;
  }

  .navbar {
      ul {
        li {
          a {
            font-size: 18px;
            margin: 0 12px;
          }
        } 
      } 
    }
    .right_nav {
      padding-left: 0;
      li {
        .theme_btn {
          padding: 0;
          width: 150px;
          text-align: center;
        }
      }
    } 
  } 
  .searchForm {
    padding-left: 15px;
    padding-right: 15px;
  }
}
@media (max-width: 991px) {
  .main_header_area {
    padding: 20px 4.4795rem 0;
  }
  .navbar {
    .right_nav {
      li {
        a {
          padding: 0 10px;
          margin-right: 0;
        }
      } 
    } 
    .navbar-toggler {
      font-size: 25px;
      color: #fff;
      background: #c13520;
      padding: 0 20px;
      margin: 0;
      margin-left: 15px;
      line-height: 50px;
      border: 0;
      float: right;
      border-radius: 0;
      display: none;
    }
    .navbar_supported {
      float: none;
      width: 100%;
    }
    .navbar-nav {
      margin: 0;
      max-height: 370px;
      overflow-y: auto;
      background: #c13520;
      border: 1px dashed #fff;
      padding-left: 0 !important;
      display: none;
      li {
        border-bottom: 1px dashed #fff;
        a {
          line-height: 45px !important;
          margin: 0 20px !important;
          color: #fff !important;
        }
        &:last-child {
          border: 0;
        }
        &::after {
          display: none !important;
        }
        &::before {
          display: none;
        }
      }
      li.dropdown {
        position: relative;
        &::after {
          content: "\f103";
          position: absolute;
          top: 0;
          right: 20px;
          font: normal normal normal 15px/48px FontAwesome;
          color: #fff;
        }
        .dropdown-menu {
          background-color: transparent;
          border: 0;
          border-top: 1px dashed #fff;
          border-radius: 0 !important;
          padding: 0 !important;
          box-shadow: none !important;
          &::before {
            display: none;
          }
          li {
            a {
              border: 0;
              color: #fff;
              margin: 0 10px;
              &:hover, &:focus, &.active {
                color: #fff;
                border: 0;
                padding: 0 20px;
              }
            }
          }
          .dropdown:after {
            display: none;
          }
        }
      }
    }
  } 
  a.theme_btn {
    background: transparent !important;
    text-align: left !important;
    color: #fff;
    background-image: none;
  }
  @media (max-width : 768px) {
    .main_header_area {
      padding: 20px 1rem 0;
    }
  }
  @media (max-width : 576px) {
    .main_header_area {
      padding: 20px 1rem 0;
    }
    #interactive-circle {
      position: absolute;
      top: 50%;
      left: 33%;
    }
    .navbar {
      .right_nav {
        li {
          a.theme_btn {
            padding: 0;
            width: 130px;
          }
        }
      } 
    } 
  }
`;

export const Overlay = styled.div`
  position: fixed;
  background: rgba(0, 0, 0, 0.7);
  width: 100%;
  height: 100%;
  display: none;
  transition: 0.4s;

  ${({ sidebar }) =>
    sidebar &&
    `
      display: block;
      z-index: 4;
  `}
`;
