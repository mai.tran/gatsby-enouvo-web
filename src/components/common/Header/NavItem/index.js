import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { SubNavItemWrapper, NavItemWrapper } from './styles';

const NavItem = ({ item }) => {
  const isDropdown = item.subNav && item.subNav.length > 0;

  return (
    <NavItemWrapper className={isDropdown ? 'dropdown' : ''}>
      <Link
        activeClassName="active"
        className={isDropdown ? 'nav-link dropdown-toggle' : ''}
        href={item.href}
        to={item.href}
        role="button"
        data-toggle={isDropdown ? 'dropdown' : ''}
      >
        {item.title}
      </Link>
      {isDropdown && <SubNavItem subNavs={item.subNav} />}
    </NavItemWrapper>
  );
};

const SubNavItem = ({ subNavs, theme }) => (
  <SubNavItemWrapper className="dropdown-menu">
    {subNavs.map(subNav => (
      <li
        key={subNav.title}
        className={subNav.subNav && subNav.subNav.length > 0 ? 'dropdown' : ''}
      >
        <a
          to="/"
          activeClassName="current"
          className="nav-link dropdown-toggle"
          href={subNav.href}
        >
          {subNav.title}
        </a>
        {subNav.subNav && subNav.subNav.length > 0 && (
          <SubNavItem subNavs={subNav.subNav} />
        )}
      </li>
    ))}
  </SubNavItemWrapper>
);

SubNavItem.propTypes = {
  subNavs: PropTypes.array,
  theme: PropTypes.string,
};

NavItem.propTypes = {
  item: PropTypes.object,
};

export default NavItem;
