import styled from 'styled-components';

export const NavItemWrapper = styled.li`
  & > a {
    font-family: 'Poppins-Medium' !important;
    font-size: 16px !important;
    margin: 0 12.5px;
    color: ${({ theme }) => theme.text[0]} !important;
    &.active,
    &:hover,
    &:focus {
      color: ${({ theme }) => theme.primary[0]} !important;
    }
  }
`;

export const SubNavItemWrapper = styled.ul`
  a:focus,
  a:hover {
    color: ${({ theme }) => theme.secondary[0]} !important;
    &::before {
      background: ${({ theme }) => theme.secondary[0]} !important;
    }
  }
`;
